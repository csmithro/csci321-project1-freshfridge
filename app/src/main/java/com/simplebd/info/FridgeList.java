package com.simplebd.info;

import java.util.List;

import com.example.freshfridge.R;
import com.example.freshfridge.R.id;
import com.example.freshfridge.R.layout;
import com.example.freshfridge.R.menu;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.os.Build;

public class FridgeList extends Activity {

	ListView foodList;
	TextView type;

	DatabaseHelper db = new DatabaseHelper(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fridge_list);

		foodList = new ListView(this);
		type = new TextView(this);

		foodList = (ListView)findViewById(R.id.list_item);
		type = (TextView)findViewById(R.id.textView1);

		Intent data = getIntent();
		String listType = data.getStringExtra("ListType");
		List<FoodItem> listOfItems = db.getAllItems();
		ArrayAdapter arrayAdapter = new ArrayAdapter<FoodItem>(FridgeList.this,android.R.layout.simple_list_item_1, listOfItems);
        foodList.setAdapter(arrayAdapter);
		switch (listType.charAt(0)){
			case 'A'://ALL
				break;
			case 'V': //"VEGGIES"
				break;
			case 'F': //"FRUIT"
				break;
			case 'D': //"DAIRY"
				break;
			case 'M': //"MEAT"
				break;
			case 'O': //"OTHER"
				break;

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.fridge_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
