package com.simplebd.info;

import com.example.freshfridge.R;
import com.example.freshfridge.R.id;
import com.example.freshfridge.R.layout;
import com.example.freshfridge.R.menu;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

/*
 * Class FridgeItemInfo displays the info of an item in the users fridge list.
 */

public class FridgeItemInfo extends Activity {

    /*
     * Method creates the activity.
     */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fridge_item_info);

		
	}

    /*
     * Method creates the menu, the action bar if it is present.
     */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.fridge_item_info, menu);
		return true;
	}

    /*
     * Method is used to handle action bar item clicks.
     */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


}
