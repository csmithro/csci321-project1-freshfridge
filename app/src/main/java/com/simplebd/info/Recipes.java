package com.simplebd.info;

/*
 * Recipe class is used to create the Recipes Activity and display a list of items
 * inside the Recipes and allow you to add new items.
 */

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.freshfridge.R;

import java.util.Vector;

public class
        Recipes extends ListActivity {
    Vector<Group> recipeList;

    //Hard Coded URLS for recipes
    String veganBreakfastURL = "http://www.food.com/search/vegan+breakfast";
    String veganLunchURL = "http://www.food.com/search/vegan+lunch";
    String veganDinnerURL = "http://allrecipes.com/recipes/1661/everyday-cooking/vegan/main-dishes/";
    String veganSnacksURL = "http://www.peta.org/living/food/making-transition-vegetarian/everyday-eating/quick-easy-snacks/";

    String carnivoreBreakfastURL = "http://breakfast.food.com/";
    String carnivoreLunchURL = "http://lunch.food.com/popular";
    String carnivoreDinnerURL = "http://www.food.com/package/quick-and-easy-dinners";
    String carnivoreSnacksURL = "http://www.food.com/topic/snacks/popular";

    String vegetarianBreakfastURL = "http://www.food.com/topic/vegetarian-breakfast";
    String vegetarianLunchURL = "http://www.food.com/topic/vegetarian-lunch-snacks";
    String vegetarianDinnerURL = "http://www.food.com/topic/vegetarian";
    String vegetarianSnacksURL = "http://www.vegetariantimes.com/recipe/snacks/";

    String barbecueURL = "http://www.food.com/topic/barbecue";

    /*
    * "Constructor" method
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        recipeList = new Vector<Group>(1);

        //Added children for Vegan
        recipeList.add(new Group("Vegan"));

        recipeList.get(0).add("Breakfast", veganBreakfastURL);
        recipeList.get(0).add("Lunch", veganLunchURL);
        recipeList.get(0).add("Dinner", veganDinnerURL);
        recipeList.get(0).add("Snack", veganSnacksURL);

        //Added children for Vegetarian
        recipeList.add(new Group("Vegetarian"));

        recipeList.get(1).add("Breakfast", vegetarianBreakfastURL);
        recipeList.get(1).add("Lunch", vegetarianLunchURL);
        recipeList.get(1).add("Dinner", vegetarianDinnerURL);
        recipeList.get(1).add("Snack", vegetarianSnacksURL);

        //Added children for Carnivore
        recipeList.add(new Group("Carnivore"));

        recipeList.get(2).add("Breakfast", carnivoreBreakfastURL);
        recipeList.get(2).add("Lunch", carnivoreLunchURL);
        recipeList.get(2).add("Dinner", carnivoreDinnerURL);
        recipeList.get(2).add("Snack", carnivoreSnacksURL);

        //Added children for Barbecue
        recipeList.add(new Group("Barbecue"));
        recipeList.get(3).add("Barbecue", barbecueURL);


        ExpandableListView listView = (ExpandableListView)
                findViewById(android.R.id.list);
        RecipesAdapter ra = new RecipesAdapter(this, recipeList);
        listView.setAdapter(ra);

    }

    /*
     * Method is used to inflate the action bar menu.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.recipes, menu);
        return true;
    }


    /*
     * Method is used to create items on the action bar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /*
     * Method is used when an item in the list is clicked on.
     * Sends a toast with the item's name
     * NEED TO ADD IN POPOVER OR DISPLAY TO DISPLAY INFO ON ITEM *****
     */
    @Override
    protected void onListItemClick(ListView l, View v, int
            position, long id) {
        String item = (String) getListAdapter().getItem
                (position);
        Toast.makeText(this, item + " selected",
                Toast.LENGTH_LONG).show();
    }

    /*
     * Class is used to create a new group to create each category
     */
    public class Group {
        private String name;
        private Vector<Recipe> recipes;
        private int size;
        public Group(String _name) {
            name = _name;
            //format: recipes<Label, URL>
            recipes = new Vector<Recipe>(1);
        }

        //Adds new recipe to Vector
        protected void add(String _label, String _url)
        {
            recipes.add(new Recipe(_label, _url));
            size++;
            return;
        }

        //Returns an individual recipe
        protected Recipe get(int i)
        {
            return recipes.get(i);
        }

        //returns the size of the recipe vector
        protected int getSize()
        {
            return size;
        }
    }

    /*
    * This class contains the label for the recipe, and its URL
     */
    public class Recipe {
        String label, url;
        public Recipe(String _label, String _url)
        {
            label = _label;
            url = _url;
        }

        protected String getLabel()
        {
            return label;
        }

        protected String getUrl()
        {
            return url;
        }
    }

    public class RecipesAdapter extends
            BaseExpandableListAdapter{
        public Vector<Group> groups;
        public LayoutInflater inflater;
        public Activity act;

        public RecipesAdapter(Activity a, Vector<Group> g) {
            act = a;
            groups = g;
            inflater = act.getLayoutInflater();
        }

        /*
         * Method is used to get the categories positions and return it
         */
        @Override
        public Recipe getChild(int groupPosition, int
                childPosition) {
            return groups.get(groupPosition).get(childPosition);
        }

        /*
         * Method is used by ListActivity to get the ChildID
         */
        @Override
        public long getChildId(int groupPosition, int
                childPosition) {
            return 0;
        }

        /*
         * Method is used by ListActivity to get the ChildView (ConvertView)
         * which is used to display the list view.
         */
        @Override
        public View getChildView(final int groupPosition, final int
                childPosition,
                                 boolean isLastChild, View
                                         convertView, ViewGroup parent) {
            final String children = getChild(groupPosition, childPosition).getLabel();
            //            System.out.println("THIS IS MY CHILD : " + children);
            TextView text = null;
            if (convertView == null) {
                convertView = inflater.inflate
                        (R.layout.listrow_group, null);
            }
            text = (TextView) convertView.findViewById(R.id.textView100);
            text.setText(children);
            convertView.setOnClickListener(new
                                                   View.OnClickListener() {
                                                       @Override
                                                       public void onClick(View v) {
                                                           //This is where the url will be called
                                                            openURL(getChild(groupPosition, childPosition).getUrl());
                                                       }
                                                   });
            return convertView;
        }

        /*
        *   This method opens a webpage from the url provided
         */
        protected void openURL(String _url)
        {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(_url));
            startActivity(browserIntent);
            return;
        }


        /*
         * Method is used to get the children (Categories items) count
         */
        @Override
        public int getChildrenCount(int groupPosition) {
            return groups.get(groupPosition).getSize();
        }

        /*
         * Method is used to get the groups (Categories)
         */
        @Override
        public Object getGroup(int groupPosition) {
            return groups.get(groupPosition);
        }

        /*
         * Method is used to get the group (Categories) count
         */
        @Override
        public int getGroupCount() {
            return groups.size();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
            super.onGroupCollapsed(groupPosition);
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
            super.onGroupExpanded(groupPosition);
        }

        /*
         * Method required by ListActivity
         */
        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        /*
         * Method is required by ListActivity and used to connect the ConvertView
         * to the listrow_group.xml file
         */
        @Override
        public View getGroupView(int groupPosition, boolean
                isExpanded,
                                 View convertView, ViewGroup
                                         parent) {
            if (convertView == null) {
                convertView = inflater.inflate
                        (R.layout.listrow_group, null);
            }
            Group group = (Group) getGroup(groupPosition);
            ((CheckedTextView) convertView).setText
                    (group.name);
            ((CheckedTextView) convertView).setChecked
                    (isExpanded);
            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int
                childPosition) {
            return false;
        }
    }
}