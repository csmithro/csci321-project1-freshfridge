package com.simplebd.info;

/*
 * Fridge class is used to create the Fridge Activity and display a list of items
 * inside the Fridge and allow you to add new items.
 */

import com.example.freshfridge.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;


import java.util.List;
import java.util.Vector;

public class Fridge extends ListActivity {
    Vector<Group> fridge;

    Button addItem;
    ImageButton mainScreenButton;

    /*
     * Method is used to create the ListActivity
     * Assigns the buttons to their variables
     * and greats the Groups / Names them.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fridge);

        addItem = new Button(this);

        addItem = (Button)findViewById(R.id.add_item);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        fridge = new Vector<Group>(1);
        fridge.add(new Group("Vegetable"));
        fridge.add(new Group("Fruit"));
        fridge.add(new Group("Dairy"));
        fridge.add(new Group("Meat"));
        fridge.add(new Group("Other"));

        ExpandableListView listView = (ExpandableListView)
                findViewById(android.R.id.list);
        FridgeAdapter fa = new FridgeAdapter(this, fridge);
        listView.setAdapter(fa);

        populateGroups();
    }

    /*
     * Method is used to inflate the action bar menu.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.fridge, menu);
        return true;
    }

    /*
     * Method uses a switch statement to see which button was clicked on the screen
     * to display or do a action.
     */
//    public void btnClick(View v)
//    {
//        switch(v.getId()) {
//            case R.id.add_item:
//                Intent addIntent = new Intent
//                        (getApplicationContext(), AddItem.class);
//                startActivity(addIntent);
//                break;
////            case R.id.imageButton1:
////                addIntent = new Intent(getApplicationContext(),
////                        Settings.class);
////                startActivity(addIntent);
////                break;
////            case R.id.imageButton2:
////                addIntent = new Intent(getApplicationContext(),
////                        MainScreen.class);
////                startActivity(addIntent);
////                break;
///*            case R.id.imageButton3:
//                //Doesn't do anything. Is supposed to return to
//previous page
//                this.finish();
//                break;
//*/          default:
//                break;
//        }
//    }
//    View.OnClickListener myhandler = new
//            View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    btnClick(v);
//                }
//            };

    /*
     * Method is used to create items on the action bar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.add_item) {
                Intent i = new Intent(getApplicationContext(), AddItem.class);
                startActivity(i);
                unpopulateGroups();
                populateGroups();
                return true;
        }
        else if(id == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /*
     * Method is used when an item in the list is clicked on.
     * Sends a toast with the item's name
     * NEED TO ADD IN POPOVER OR DISPLAY TO DISPLAY INFO ON ITEM *****
     */
    @Override
    protected void onListItemClick(ListView l, View v, int
            position, long id) {
        String item = (String) getListAdapter().getItem
                (position);
        Toast.makeText(this, item + " selected",
                Toast.LENGTH_LONG).show();
    }

    /*
     * Method is used to addItems from the fridge vector to the item vector
     */
    protected void addItem(FoodItem f) {
        for(int i = 0; i < fridge.size(); i++) {
            if (f.getType().equals(fridge.get(i).name)) {
                fridge.get(i).item.add(f);
                return;
            }
        }
        for(int i = 0; i < fridge.size(); i++)
            if("Other".equals(fridge.get(i).name)) {
                fridge.get(i).item.add(f);
                return;
            }
    }

    /*
    * This methods the whole item from the database
     */
    protected void deleteItem(FoodItem f)
    {
        DatabaseHelper db = new DatabaseHelper(this);
        db.deleteItem(f);
        //This refreshes the page
        Intent addIntent = new Intent
                (getApplicationContext(), Fridge.class);
        startActivity(addIntent);

        return;
    }

    /*
    * This method subtracts 1 from the quantity of the item
     */
    protected void deductFromItem(FoodItem f, int i)
    {
        DatabaseHelper db = new DatabaseHelper(this);
        db.deductFromItem(f, i);
        return;

    }

    protected void addToItem(FoodItem f, int i) {
        DatabaseHelper db = new DatabaseHelper(this);
        db.deductFromItem(f, -i);
        return;    }

    /*
    * This method subtracts 1 from the quantity of the item
     */
    protected void addOneToItem(FoodItem f)
    {
        DatabaseHelper db = new DatabaseHelper(this);
        db.deductFromItem(f, -1);
        return;
    }


    /*
     * Method is used to populate the categories when user loads activity
     */
    protected void populateGroups() {
        DatabaseHelper db = new DatabaseHelper(this);
        List<FoodItem> fl = db.getAllItems();

        //Sorts by Expiration date
/*        List<FoodItem> tempFl = fl;
        fl.clear();
        for(int i = 0; i < tempFl.size(); i++)
        {
            FoodItem smallest = tempFl.get(i);
            for(int j = 0; j < tempFl.size(); i++)
            {
                //Determines which item has the latest expiration date
                if(Integer.parseInt(smallest.getExpirationDate().substring(6, 8)) >=
                        Integer.parseInt(tempFl.get(j).getExpirationDate().substring(6,8))) {
                    if (Integer.parseInt(smallest.getExpirationDate().substring(3, 5)) >=
                            Integer.parseInt(tempFl.get(j).getExpirationDate().substring(3, 5))) {
                        if (Integer.parseInt(smallest.getExpirationDate().substring(0, 2)) >=
                                Integer.parseInt(tempFl.get(j).getExpirationDate().substring(0, 2))) {
                            smallest = tempFl.get(j);
                        }
                    }
                }

            }
            fl.add(smallest);
        }
*/
        for(int i = 0; i < fl.size(); i++)
        {
            //This assumes that the Quantity is an integer
            //If the quantity of the item is less than 1 it deletes the item.
            if(Integer.parseInt(fl.get(i).getQuantity()) > 0)
            {
                addItem(fl.get(i));
            }
            else if(Integer.parseInt(fl.get(i).getQuantity()) <= 0)
            {
                deleteItem(fl.get(i));
            }
        }
        return;
    }
    
    /*
     * Unpopulates the fridge, this is called whenever a new item is added to the fridge.
     * All items will be removed and then all items in the DB will be added.
     * Used to stop duplicates.
     */
    protected void unpopulateGroups() {
        for(int i = 0; i < fridge.size(); i++)
                fridge.get(i).unpopulate();
    }

    /*
     * Class is used to create a new group to create each category
     */
    public class Group {
        public String name;
        public Vector<FoodItem> item;
        public Group(String _name) {
            name = _name;
            item = new Vector<FoodItem>(1);
        }

        public void unpopulate() {
            item.removeAllElements();
        }
    }

    /*
     * FridgeAdapter class is used with the groups vector to create the categories
     * and assign them their values correctly.
     */
    public class FridgeAdapter extends
            BaseExpandableListAdapter{
        public Vector<Group> groups;
        public LayoutInflater inflater;
        public Activity act;

        public FridgeAdapter(Activity a, Vector<Group> g) {
            act = a;
            groups = g;
            inflater = act.getLayoutInflater();
        }

        /*
         * Method is used to get the categories positions and return it
         */
        @Override
        public FoodItem getChild(int groupPosition, int
                childPosition) {
            return groups.get(groupPosition).item.get
                    (childPosition);
        }

        /*
         * Method is used by ListActivity to get the ChildID
         */
        @Override
        public long getChildId(int groupPosition, int
                childPosition) {
            return 0;
        }

        /*
         * Method is used by ListActivity to get the ChildView (ConvertView)
         * which is used to display the list view.
         */
        @Override
        public View getChildView(final int groupPosition, final int
                childPosition,
                                 boolean isLastChild, View
                                         convertView, ViewGroup parent) {
            final String children = getChild(groupPosition, childPosition).getQuantity() + "   "
                    + getChild(groupPosition, childPosition).getName();
            //            System.out.println("THIS IS MY CHILD : " + children);
            final String expirationDate = getChild(groupPosition, childPosition).getExpirationDate();
            TextView text = null;
            if (convertView == null) {
                convertView = inflater.inflate
                        (R.layout.listrow_group, null);
            }
            text = (TextView) convertView.findViewById(R.id.textView100);
            text.setText(children);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(act, children + "\nexpire on:\n" + expirationDate, Toast.LENGTH_SHORT).show();

                    final Dialog d = new Dialog(Fridge.this);
                    d.setTitle(getChild(groupPosition, childPosition).getName() + " expires on " + getChild(groupPosition, childPosition).getExpirationDate() +
                    "\nAmount: " + getChild(groupPosition, childPosition).getQuantity());
                    d.setContentView(R.layout.dialog);

                    Button b1 = (Button) d.findViewById(R.id.button1);
                    Button b2 = (Button) d.findViewById(R.id.button2);

                    final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
                    np.setMaxValue(100); // max value 100
                    np.setValue(Integer.parseInt(getChild(groupPosition, childPosition).getQuantity()));
                    np.setMinValue(0);   // min value 0
                    np.setWrapSelectorWheel(false);
                    np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                        @Override
                        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

                        }
                    });
                    b1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Set
                            int set = np.getValue();
                            int cur = Integer.parseInt(getChild(groupPosition, childPosition).getQuantity());
                            if(set == 0) deleteItem(getChild(groupPosition, childPosition));
                            else if(cur < set) addToItem(getChild(groupPosition, childPosition), set-cur);
                            else deductFromItem(getChild(groupPosition, childPosition), cur-set);
                            d.dismiss();
                            unpopulateGroups();
                            populateGroups();
                            finish();
                            Intent addIntent = new Intent(getApplicationContext(), Fridge.class);
                            startActivity(addIntent);
                        }
                    });
                    b2.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v) {
                            //Cancel
                            d.dismiss();
                             // dismiss the dialog
                        }
                    });
                    d.show();                }
            });
            return convertView;
        }

        /*
         * Method is used to get the children (Categories items) count
         */
        @Override
        public int getChildrenCount(int groupPosition) {
            return groups.get(groupPosition).item.size();
        }

        /*
         * Method is used to get the groups (Categories)
         */
        @Override
        public Object getGroup(int groupPosition) {
            return groups.get(groupPosition);
        }

        /*
         * Method is used to get the group (Categories) count
         */
        @Override
        public int getGroupCount() {
            return groups.size();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
            super.onGroupCollapsed(groupPosition);
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
            super.onGroupExpanded(groupPosition);
        }

        /*
         * Method required by ListActivity
         */
        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        /*
         * Method is required by ListActivity and used to connect the ConvertView
         * to the listrow_group.xml file
         */
        @Override
        public View getGroupView(int groupPosition, boolean
                isExpanded,
                                 View convertView, ViewGroup
                                         parent) {
            if (convertView == null) {
                convertView = inflater.inflate
                        (R.layout.listrow_group, null);
            }
            Group group = (Group) getGroup(groupPosition);
            ((CheckedTextView) convertView).setText
                    (group.name);
            ((CheckedTextView) convertView).setChecked
                    (isExpanded);
            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int
                childPosition) {
            return false;
        }
    }
}