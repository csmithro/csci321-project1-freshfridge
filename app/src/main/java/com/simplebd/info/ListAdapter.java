package com.simplebd.info;

import com.example.freshfridge.R;
import com.example.freshfridge.R.id;
import com.example.freshfridge.R.layout;
import com.example.freshfridge.R.menu;
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

/*
 * Class ListAdapter is used to help with creating the list.
 */

public class ListAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<FoodItem> objects;
    boolean checked = false;

    ListAdapter(Context context, ArrayList<FoodItem> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * Method is sued to get the view.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.grocery_list_checkbox, parent, false);
        }

        FoodItem p = getProduct(position);

        ((TextView) view.findViewById(R.id.textMain)).setText(p.getName());
        CheckBox cbBuy = (CheckBox) view.findViewById(R.id.cbBox);
        cbBuy.setOnCheckedChangeListener(myCheckChangList);
        cbBuy.setTag(position);
        cbBuy.setChecked(p.box);
        return view;
    }

    FoodItem getProduct(int position) {
        return ((FoodItem) getItem(position));
    }

    ArrayList<FoodItem> getBox() {
        ArrayList<FoodItem> box = new ArrayList<FoodItem>();
        for (FoodItem p : objects) {
            if (p.box)
                box.add(p);
        }
        return box;
    }

    OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                boolean isChecked) {
            getProduct((Integer) buttonView.getTag()).box = isChecked;
        }
    };
}

