package com.simplebd.info;

/**
 * Created by Christian Valderrama on 9/25/15.
 */
public class GroceryItem {
    //Properties
    int _id;
    String _name;
    String _date_purchased;
    int _quantity;
    int _is_on_glist;
    boolean box;

    //Emtpy constructor
    public GroceryItem()
    {

    }
    //
    // Constructor assigns all the values being passed in.
    //
    public GroceryItem(int id, String name, String date_purchased, int quantity, int is_on_glist)
    {
        this._id = id;
        this._name = name;
        this._date_purchased = date_purchased;
        this._quantity = quantity;
        this._is_on_glist = is_on_glist;
    }
    //
    // Constructor assigns all the values being passed in.
    //
    public GroceryItem( String name, String date_purchased, int quantity, int is_on_glist)
    {
        this._name = name;
        this._date_purchased = date_purchased;
        this._quantity = quantity;
        this._is_on_glist = is_on_glist;
    }
    //
    // Constructor assigns all the values being passed in.
    //No is_on_glist so we can use in AddGroceryItem class
    //
    public GroceryItem(int id, String name, String date_purchased, int quantity)
    {
        this._id = id;
        this._name = name;
        this._date_purchased = date_purchased;
        this._quantity = quantity;
    }

    //Getters
    public int getID() {return this._id;}
    public String getName() {return this._name;}
    public String getDataPurchased() {return this._date_purchased;}
    public int getQuantity() {return this._quantity;}
    public int getIsOnGlist() {return this._is_on_glist;}

    //Setters
    public int setID(int id) {return this._id = id;}
    public String setName(String name) {return this._name = name;}
    public String setDatePurchased(String date_purchased) {return this._date_purchased  = date_purchased;}
    public int setQuantity(int quantity) {return this._quantity = quantity;}

    //Set if it is on the grocery list
    public void setIsOnGlist(int is_on_glist)
    {
        //Check to see if the item is on the grocery list
        if(is_on_glist == 0)
        {
            this.box = false;
        }else {
            this.box = true;
        }
        this._is_on_glist = is_on_glist;
    }
}
