package com.simplebd.info;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.freshfridge.R;

import java.io.IOException;

/**
 * Created by ChristianValderrama on 9/25/15.
 */
public class AddGroceryItem extends Activity {

    //Properties
    EditText myNameText;
    Spinner mySpinner;
    GroceryItem item;

    DatabaseHelperGroceryList db = new DatabaseHelperGroceryList(this); //Create database variable

    /*
     * Method is used to Create the Activity and assign the spinner to its values / variable.
     * Also gets the text fields to their variables.
     */
    public void onCreate(Bundle savedInstanceBundle)
    {
        super.onCreate(savedInstanceBundle);

        setContentView(R.layout.activity_add_grocery_item);

        //Setup back button
        getActionBar().setDisplayHomeAsUpEnabled(true);

        mySpinner = (Spinner) findViewById(R.id.quantity_spinner);
        //Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.quantity_picker, android.R.layout.simple_spinner_item);
        //Specifiy the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Apply the adapter to the spinner
        mySpinner.setAdapter(adapter);

        myNameText = (EditText) findViewById(R.id.name);

    }

    /*
     * Method is used to inflate action bar items.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_item, menu);
        return true;
    }

    /*
 * Method is used to create items on the action bar
 */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        System.out.println("IN HERE");
        if (id == R.id.save_button) {
            btnClick();
            return true;
        }else if (id == android.R.id.home) {
            finish();
//			return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
     * Method is used when the save button is clicked to grab all the information entered
     * and send it to the FoodItem class so we can save it.
     */
    public void btnClick()
    {
        try
        {
            if(myNameText.length() <= 0)
            {
                IOException e = new IOException();
                throw e;
            }
        }
        catch (Exception e)
        {
            incorrectDataEntryDialogBox("Please enter a name");
            return;
        }
        Intent i = new Intent(this, GroceryList.class);
        String nameStr = myNameText.getText().toString();
        String quantityStr = mySpinner.getSelectedItem().toString();
        int id = 0;
        for(int x = 0; x > 0; x++)
        {
            id = x;
        }

        item = new GroceryItem(id, nameStr, "00/00/00", Integer.parseInt(quantityStr));
        db.addItem(item);
        i.putExtra("ListType", "ALL");
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        onContentChanged();
        startActivity(i);

    }
    /*
	 * Method is used to make sure the entry datas are entered correctly.
	 */
    public void incorrectDataEntryDialogBox(String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(message);
        dialog.setCancelable(true);
        dialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //dialog.cancel();
                        //This should return the user to the addItem
                    }
                });
        AlertDialog alert = dialog.create();
        alert.show();
    }

}
