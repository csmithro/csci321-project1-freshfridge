package com.simplebd.info;

/*
 * AddItem class is used to add a new item to the fridge list to display in the Fridge class.
 */
import com.example.freshfridge.R;

import android.app.Activity;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.os.Build;

import java.io.IOException;

public class AddItem extends Activity {

	EditText name;
	Spinner type;
	EditText quantity;
	EditText datePurchased;
	EditText expirationDate;		//Declared necessary widgets

	Button addItem;

	DatabaseHelper db = new DatabaseHelper(this);		//Opened db		
	FoodItem item;
	int spinnerPosition;

	/*
	 * Create method is the constructor for the Activity.
	 * Grabs the addItem on action bar, grabs all text fields.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_item);

		name = new EditText(this);
		type = new Spinner(this);
		quantity = new EditText(this);
		datePurchased = new EditText(this);
		expirationDate = new EditText(this);

		addItem = new Button(this);

		//Set back button on action bar
		getActionBar().setDisplayHomeAsUpEnabled(true);

		name = (EditText)findViewById(R.id.editText1);
		type = (Spinner)findViewById(R.id.spinner01);
		quantity = (EditText)findViewById(R.id.editText3);
		datePurchased = (EditText)findViewById(R.id.editText4);
		expirationDate = (EditText)findViewById(R.id.editText5);		//

		addItem = (Button)findViewById(R.id.save_button);

		type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				Object item = parent.getItemAtPosition(pos);
				spinnerPosition =pos;
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

		});

	}

	/*
     * Method is used when the save button is clicked to grab all the information entered
     * and send it to the FoodItem class so we can save it. Also does checks to make sure
     * entries are entered correctly.
     */
	public void btnClick()
	{
		Intent i = new Intent(this, Fridge.class);
		String nameStr = name.getText().toString();
		if(nameStr.length()<=0)
		{
			incorrectDataEntryDialogBox("Please enter a name for the item.");
			return;
		}
		String typeStr = type.getSelectedItem().toString();
		String quantityStr = quantity.getText().toString();
		try
		{
			if (Integer.parseInt(quantityStr) < 1)
			{
				incorrectDataEntryDialogBox("The Quantity you enter \nmust be an integer\n" +
						"greater than 0");
				return;
			}

		}
		catch (Exception e)
		{
			incorrectDataEntryDialogBox("The Quantity you enter \nmust be an integer");
			return;
		}
		String datePurchasedStr = datePurchased.getText().toString();
		try
		{
			if(datePurchasedStr.length() != 8)
			{
				IOException e = new IOException();
				throw e;
			}
			//This makes sure that they are in the format mm/dd/yy
			if(Integer.parseInt(datePurchasedStr.substring(0,2)) >= 1
					&& Integer.parseInt(datePurchasedStr.substring(0,2)) <= 12
					&& datePurchasedStr.substring(2,3).equals("/")
					&& Integer.parseInt(datePurchasedStr.substring(3,5)) >= 1
					&& Integer.parseInt(datePurchasedStr.substring(3,5)) <= 12
					&& datePurchasedStr.substring(5,6).equals("/")
					&& Integer.parseInt(datePurchasedStr.substring(6,8)) >= 1
					&& Integer.parseInt(datePurchasedStr.substring(6,8)) <= 12)
			{
				//It is in the right format
			}
		}
		catch (Exception e)
		{
			incorrectDataEntryDialogBox("The Format of the date \nmust be mm/dd/yy\n" +
					"e.g. 01/01/01");
			return;
		}
		String expirationDateStr = expirationDate.getText().toString();
		try
		{
			if(expirationDateStr.length() != 8)
			{
				IOException e = new IOException();
				throw e;
			}
			//This makes sure that they are in the format mm/dd/yy
			if(Integer.parseInt(expirationDateStr.substring(0,2)) >= 1
					&& Integer.parseInt(expirationDateStr.substring(0,2)) <= 12
					&& expirationDateStr.substring(2,3).equals("/")
					&& Integer.parseInt(expirationDateStr.substring(3,5)) >= 1
					&& Integer.parseInt(expirationDateStr.substring(3,5)) <= 12
					&& expirationDateStr.substring(5,6).equals("/")
					&& Integer.parseInt(expirationDateStr.substring(6,8)) >= 1
					&& Integer.parseInt(expirationDateStr.substring(6,8)) <= 12)
			{
				//It is in the right format
			}
		}
		catch (Exception e)
		{
			incorrectDataEntryDialogBox("The Format of the date \nmust be mm/dd/yy\n" +
					"e.g. 01/01/01");
			return;
		}

		//This section confirms that the expiration date is after the purchase date
		try
		{
			//If the expiration year is not greater than the purchase year
			if(!(Integer.parseInt(expirationDateStr.substring(6,8)) >= Integer.parseInt(datePurchasedStr.substring(6,8))))
			{
				//If the expiration month is not greater than the purchase month
				if(!(Integer.parseInt(expirationDateStr.substring(3,5)) >= Integer.parseInt(datePurchasedStr.substring(3,5))))
				{
					if(!(Integer.parseInt(expirationDateStr.substring(0,2)) >= Integer.parseInt(datePurchasedStr.substring(0,2))))
					{
						IOException e = new IOException();
						throw e;
					}
				}
			}

		}
		catch (Exception e)
		{
			incorrectDataEntryDialogBox("The Purchase Date is after \nthe expiration date. " +
					"\nPlease reenter the information");
			return;
		}

		item = new FoodItem(name.getText().toString(), type.getSelectedItem().toString(), quantity.getText().toString(),
				datePurchased.getText().toString(), expirationDate.getText().toString());
		db.addItem(item);
		i.putExtra("ListType", "ALL");
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);

	}


	View.OnClickListener myhandler = new View.OnClickListener() {

		/*
         * Method is used to call btnClick to handle the save button event.
         */
		@Override
		public void onClick(View v) {
			btnClick();
		}
	};

	/*
     * Method is used to inflate action bar items.
     */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_item, menu);
		return true;
	}

	/*
 * Method is used to create items on the action bar
 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		System.out.println("IN HERE");
		if (id == R.id.save_button) {
			btnClick();
			return true;
		}else if (id == android.R.id.home) {
			System.out.println("IN HERE X2");
			finish();
//			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/*
	 * Method is used to make sure the entry datas are entered correctly.
	 */
	public void incorrectDataEntryDialogBox(String message) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setMessage(message);
		dialog.setCancelable(true);
		dialog.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						//dialog.cancel();
						//This should return the user to the addItem
					}
				});
		AlertDialog alert = dialog.create();
		alert.show();
	}
}
