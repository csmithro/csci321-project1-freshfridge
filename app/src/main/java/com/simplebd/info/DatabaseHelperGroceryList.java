package com.simplebd.info;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ChristianValderrama on 9/25/15.
 * DatabaseHelperGroceryList is sued to save new food items to a SQLite database on the phone itself.
 */
public class DatabaseHelperGroceryList extends SQLiteOpenHelper{

        //Properties
        private static final int DATABASE_VERSION = 1;

        static final String DATABASE_NAME ="myGroceryList";							//Query variables

        static final String TABLE_GROCERY_LIST = "GroceryList";
        static final String KEY_ID_LIST = "id";
        static final String KEY_NAME_LIST = "name";
        static final String KEY_PURCHASE_DATE_LIST = "purchase_date";
        static final String KEY_QUANTITY = "quantity";
        static final String KEY_IS_ON_GLIST_GROCERY = "grocery_list";

        static final String viewFoods ="ViewFoods";

        public DatabaseHelperGroceryList(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);

        }

        //Create Database Table
        public void onCreate(SQLiteDatabase db){

            // catch ilegal state exception when database is called recursively
            //Christian Smithroat
            try {
                db = getWritableDatabase();
            } catch (IllegalStateException s) {
                Log.wtf("Error", "IllegalStatException");
            }
            String CREATE_GROCERIES_TABLE = "CREATE TABLE " + TABLE_GROCERY_LIST + " (" + KEY_ID_LIST + " INTEGER PRIMARY KEY, " + KEY_NAME_LIST
                    + " TEXT, "  + KEY_PURCHASE_DATE_LIST + " TEXT, " + KEY_QUANTITY + " TEXT, " + KEY_IS_ON_GLIST_GROCERY + " INT" +")";
            db.execSQL(CREATE_GROCERIES_TABLE);
        }

        //Upgrading Database
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
            //Drop older table if exists
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROCERY_LIST);

            //Create table again
            onCreate(db);

        }


        /*
         * Method is used to add an item to the database.
         * Gets all the values first then inserts them into the new table with ContentValues();
         */
        public void addItem (GroceryItem item)
        {
            SQLiteDatabase db = this.getWritableDatabase();


            ContentValues values = new ContentValues();
            values.put(KEY_NAME_LIST, item.getName());
            values.put(KEY_PURCHASE_DATE_LIST, item.getDataPurchased());
            values.put(KEY_QUANTITY, item.getQuantity());
            values.put(KEY_IS_ON_GLIST_GROCERY, "0");		   //Retrieve values of food item

            db.insert(TABLE_GROCERY_LIST, null, values);   				   //Insert values into db
//            System.out.println("I AM PRINTING BECAUSE SAVED INTO DATABSAE: "+ values);
            db.close();												   //Close db

        }

        /*
         * Method is used to get one item. Uses a cursor in to hold the database query.
         */
        public GroceryItem getItem(int id)
        {
            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.query(TABLE_GROCERY_LIST, new String[] { KEY_ID_LIST, KEY_NAME_LIST, KEY_PURCHASE_DATE_LIST,
                            String.valueOf(KEY_IS_ON_GLIST_GROCERY) }, KEY_ID_LIST + "=?",
                    new String[] { String.valueOf(id) }, null, null, null, null);
            if (cursor != null)
                cursor.moveToFirst();

            GroceryItem item = new GroceryItem(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2),
                    Integer.parseInt(cursor.getString(3)), cursor.getInt(4));


            return item;

        }

        /*
         * Method is used to get the list of all the items in the table.
         * Uses query to access database with a cursor. Then uses the GrocreyItem
         * to be a controller to pass the data.
         */
        public List<GroceryItem> getAllItems()
        {
            List<GroceryItem> groceryItemList = new ArrayList<GroceryItem>();		//Create list items

            String selectQuery = "SELECT * FROM " + TABLE_GROCERY_LIST + ";";		//Select all items in db

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            System.out.println("THIS IS DB QUERY : " + db.rawQuery(selectQuery, null));

//            String testID = cursor.getString(0);
//            String testName = cursor.getString(1);
//            System.out.println("I AM IN HERE AND HERE IS LIST : " + cursor.getString(0));
            //loop through all rows and add to list
            if (cursor.moveToFirst()){
                do {
                    GroceryItem item = new GroceryItem();

                    item.setID(Integer.parseInt(cursor.getString(0)));
                    item.setName(cursor.getString(1));
                    item.setDatePurchased(cursor.getString(2));
                    item.setQuantity(cursor.getInt(3));
                    item.setIsOnGlist(cursor.getInt(4));

                    groceryItemList.add(item);
                }while (cursor.moveToNext());
            }

        //stop
            int temp = groceryItemList.size();
            return groceryItemList; 										//Return list of items

        }

        /*
         * Method Gets all item names in the list.
         * Query used with cursor to connect to the database and retrive the results into a
         * String ArrayList.
         */
        public List<String> getAllItemNames()
        {
            List<String> groceryItemList = new ArrayList<String>();		//Create list items

            String selectQuery = "SELECT " + KEY_NAME_LIST + " FROM " + TABLE_GROCERY_LIST;		//Select all items in db

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            //loop through all rows and add to list
            if (cursor.moveToFirst()){
                do {
                    String itemName = cursor.getString(cursor.getColumnIndex(KEY_NAME_LIST));

                }while (cursor.moveToNext());
            }

            return groceryItemList; 										//Return list of items

        }

        //Update item
        public int updateItem(GroceryItem item)
        {
            SQLiteDatabase db = this.getWritableDatabase();				//Open writable db

            ContentValues values = new ContentValues();
            values.put(KEY_NAME_LIST, item.getName());
            values.put(KEY_PURCHASE_DATE_LIST, item.getDataPurchased());
            values.put(KEY_QUANTITY, item.getQuantity());
            values.put(KEY_IS_ON_GLIST_GROCERY, item.getIsOnGlist());			//Replaces item info

            //Updating row
            return db.update(TABLE_GROCERY_LIST, values, KEY_ID_LIST + " = ?", new String[]{String.valueOf(item.getID())});


        }

        //Deduct Quantity from item
        public void deductFromItem(FoodItem item, int deductionAmount)
        {
            //This assumes that the Quantity of the item is an Integer
            //This turns the quantity into an integer, subtracts the
            //deduction amount and converts it back into a string
            String newQuantity = Integer.toString(
                    Integer.parseInt(item.getQuantity()) - deductionAmount);

            SQLiteDatabase db = this.getWritableDatabase();				//Open writable db

            String tempId = Integer.toString(item.getID());
            db.execSQL("UPDATE " + TABLE_GROCERY_LIST + " SET quantity = " + newQuantity + " where id = " + tempId + ";");
            return;
        }

        //Add to Quantity of item
        public void AddToItem(FoodItem item, int additionAmount)
        {
            //This assumes that the Quantity of the item is an Integer
            //This turns the quantity into an integer, subtracts the
            //deduction amount and converts it back into a string
            String newQuantity = Integer.toString(
                    Integer.parseInt(item.getQuantity()) + additionAmount);

            SQLiteDatabase db = this.getWritableDatabase();				//Open writable db

            String tempId = Integer.toString(item.getID());
            db.execSQL("UPDATE Groceries SET quantity = " + newQuantity + " where id = " + tempId + ";");
            return;
        }

        //Delete item from the database.
        public void deleteItem(GroceryItem item)
        {
            SQLiteDatabase db = this.getWritableDatabase();												//Open db
            db.delete(TABLE_GROCERY_LIST, KEY_ID_LIST + " = ?", new String[] { String.valueOf(item.getID())}); 	//Delete specified item
            db.close();																					//Close db
        }
        /*
         * Method is used to delete all items in the table
         */
        public void deleteAllItems()
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_GROCERY_LIST, null, null);
        }

}
