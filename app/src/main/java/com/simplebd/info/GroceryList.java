package com.simplebd.info;

import com.example.freshfridge.R;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/*
 * Class GroceryList is used to create the grocery list for the user to see.
 */

public class GroceryList extends ListActivity {

    private ArrayList<String> listValues;
//    private TextView myClickedText;
	private CheckBox myCheckbox;
    /*
	 * Method is used to create the activity.
	 * Creates back button as well as the list view.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Set layout
		setContentView(R.layout.activity_grocery_list);

		//For backbutton on action bar
		getActionBar().setDisplayHomeAsUpEnabled(true);

		//Set Checkbox
		myCheckbox = (CheckBox) findViewById(R.id.listViewCheckbox);

		listValues = new ArrayList<String>();


//        myClickedText = (TextView) findViewById(R.id.ClickedItemText);

		//Populate the list
		populateList();

		//Inflate the list adapter
		ArrayAdapter<String> myAdapter = new ArrayAdapter <String>(this,

                R.layout.listview_custom_layout, R.id.text1, listValues);


        // assign the list adapter

		setListAdapter(myAdapter);

    }

	/*
     * Method is used to addItems from the listValues array to the item vector
     */
//	protected void addItem(GroceryItem groceryItem) {
//		for(int i = 0; i < listValues.size(); i++) {
//			listValues.add(i, groceryItem._name);
////			System.out.println("INSIDE AddItem Grocery List with item : " + groceryItem._name);
//			return;
//		}
//	}

	/*
	 * Method is used when an item on the list is clicked.
	 * Gets the items position and checks the checkbox off.
	 */
    @Override
    protected void onListItemClick(ListView list, View view, int position, long id)
    {
        super.onListItemClick(list, view, position, id);

		list.setItemChecked(position, true);
    }

	/*
     * Method is used to populate the categories when user loads activity
     */
	protected void populateList() {
		System.out.println("HERE");
		DatabaseHelperGroceryList db = new DatabaseHelperGroceryList(this);
		List<GroceryItem> fl = db.getAllItems();

		for(int i = 0; i < fl.size(); i++)
		{
			//This assumes that the Quantity is an integer
			//If the quantity of the item is less than 1 it deletes the item.
//			if(fl.get(i).getQuantity() > 0)
//			{
//				System.out.println("Inside populate list here is : "+ fl.get(i).getName());
//				addItem(fl.get(i));
//			}
//			else if(fl.get(i).getQuantity() <= 0)
//			{
//				deleteItem(fl.get(i));
//			}
			listValues.add(i, fl.get(i).getName() + " - " + fl.get(i).getQuantity());

		}
		return;
	}


    @Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.grocery_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.add_item) { //Add item button
			Intent i = new Intent(getApplicationContext(), AddGroceryItem.class);
			startActivity(i);
			populateList();
			return true;
		}else if(id == android.R.id.home) //Back button
		{
			finish();
		}else if(id == R.id.delete_item_list) //Delete button
		{
			//Create an AlertDialog box to make sure they want to delete list
			new AlertDialog.Builder(this)
					.setTitle("Delete All")
					.setMessage("Are you sure you want to delete all from the list?")
					.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							//Delete all items from the table
							DatabaseHelperGroceryList db = new DatabaseHelperGroceryList(GroceryList.this);
							db.deleteAllItems();
							finish(); //Finish so that it refreshes restarting the activity!
							//This refreshes the page
							Intent addIntent = new Intent
									(getApplicationContext(), GroceryList.class);
							startActivity(addIntent);
						}
					})
					.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							//Cancel and do nothing
						}
					})
					.setIcon(android.R.drawable.ic_dialog_alert)
					.show();
		}
		return super.onOptionsItemSelected(item);
	}


}
