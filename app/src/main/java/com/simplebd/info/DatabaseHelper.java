package com.simplebd.info;

/*
 *DatabaseHelper class is used to save new food items to a SQLite database on the phone itself.
 */
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

	//Properties
	private static final int DATABASE_VERSION = 1;

	static final String DATABASE_NAME ="myGroceries";
	static final String TABLE_GROCERIES = "Groceries";
	static final String KEY_ID = "id";
	static final String KEY_NAME = "name";
	static final String KEY_CATEGORY = "category";
	static final String KEY_QUANTITY = "quantity";
	static final String KEY_PURCHASED_DATE = "purchase_date";
	static final String KEY_EXPIRATION_DATE = "expiration_date";
	static final String KEY_IS_ON_GLIST = "grocery_list";								//Query variables


	static final String viewFoods ="ViewFoods";

	public DatabaseHelper(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

	}

	//Create Database Table
	public void onCreate(SQLiteDatabase db){

		// catch illegal state exception when database is called recursively
			//Christian Smithroat
		try {
			db = getWritableDatabase();
		} catch (IllegalStateException s) {
			Log.wtf("Error", "IllegalStatException");
		}
		String CREATE_GROCERIES_TABLE = "CREATE TABLE " + TABLE_GROCERIES + " (" + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_NAME
				+ " TEXT, " + KEY_CATEGORY + " TEXT," + KEY_QUANTITY + " TEXT, " + KEY_PURCHASED_DATE + " TEXT, " + KEY_EXPIRATION_DATE +
				" TEXT, " + KEY_IS_ON_GLIST + " INT" +")";
		db.execSQL(CREATE_GROCERIES_TABLE);
	}

	//Upgrading Database
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		//Drop older table if exists
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROCERIES);

		//Create table again
		onCreate(db);

	}


	//Add food item
	public void addItem (FoodItem item)
	{
		SQLiteDatabase db = this.getWritableDatabase();


		ContentValues values = new ContentValues();
		values.put(KEY_NAME, item.getName());
		values.put(KEY_CATEGORY, item.getType());
		values.put(KEY_QUANTITY, item.getQuantity());
		values.put(KEY_PURCHASED_DATE, item.getDatePurchased());
		values.put(KEY_EXPIRATION_DATE, item.getExpirationDate());
		values.put(KEY_IS_ON_GLIST, "0");		   //Retrieve values of food item

		db.insert(TABLE_GROCERIES, null, values);   				   //Insert values into db
		db.close();												   //Close db

	}

	//Getting one item
	public FoodItem getItem(int id)
	{
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_GROCERIES, new String[] { KEY_ID, KEY_NAME, KEY_CATEGORY, KEY_QUANTITY, KEY_PURCHASED_DATE,
						KEY_EXPIRATION_DATE, String.valueOf(KEY_IS_ON_GLIST) }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		FoodItem item = new FoodItem(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3),
				cursor.getString(4), cursor.getString(5), cursor.getInt(6));
		;

		return item;

	}

	//Getting all items
	public List<FoodItem> getAllItems()
	{
		List<FoodItem> foodItemList = new ArrayList<FoodItem>();		//Create list items

		String selectQuery = "SELECT * FROM " + TABLE_GROCERIES;		//Select all items in db

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		//loop through all rows and add to list
		if (cursor.moveToFirst()){
			do {
				FoodItem item = new FoodItem();
				item.setID(Integer.parseInt(cursor.getString(0)));
				item.setName(cursor.getString(1));
				item.setType(cursor.getString(2));
				item.setQuantity(cursor.getString(3));
				item.setDatePurchased(cursor.getString(4));
				item.setExpirationDate(cursor.getString(5));
				item.setIsOnGList(cursor.getInt(6));
				foodItemList.add(item);
			}while (cursor.moveToNext());
		}

		return foodItemList; 										//Return list of items

	}

	//Get all item names
	public List<String> getAllItemNames()
	{
		List<String> foodItemList = new ArrayList<String>();		//Create list items

		String selectQuery = "SELECT " + KEY_NAME + " FROM " + TABLE_GROCERIES;		//Select all items in db

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		//loop through all rows and add to list
		if (cursor.moveToFirst()){
			do {
				String itemName = cursor.getString(cursor.getColumnIndex(KEY_NAME));

			}while (cursor.moveToNext());
		}

		return foodItemList; 										//Return list of items

	}

	//Getting item count
	public int getItemCount()
	{
		String countQuery = "SELECT * FROM " + TABLE_GROCERIES;		//Create count query
		SQLiteDatabase db = this.getReadableDatabase();				//Open readable db
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		return cursor.getCount();									//Return item count
	}

	//Update item
	public int updateItem(FoodItem item)
	{
		SQLiteDatabase db = this.getWritableDatabase();				//Open writable db

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, item.getName());
		values.put(KEY_CATEGORY, item.getType());
		values.put(KEY_QUANTITY, item.getQuantity());
		values.put(KEY_PURCHASED_DATE, item.getDatePurchased());
		values.put(KEY_EXPIRATION_DATE, item.getExpirationDate());
		values.put("KEY_IS_ON_GLIST", item.getIsOnGList());			//Replaces item info

		//Updating row
		return db.update(TABLE_GROCERIES, values, KEY_ID + " = ?", new String[]{String.valueOf(item.getID())});


	}

	//Deduct Quantity from item
	public void deductFromItem(FoodItem item, int deductionAmount)
	{
		//This assumes that the Quantity of the item is an Integer
		//This turns the quantity into an integer, subtracts the
		//deduction amount and converts it back into a string
		String newQuantity = Integer.toString(
				Integer.parseInt(item.getQuantity()) - deductionAmount);

		SQLiteDatabase db = this.getWritableDatabase();				//Open writable db

		String tempId = Integer.toString(item.getID());
		db.execSQL("UPDATE Groceries SET quantity = " + newQuantity + " where id = " + tempId + ";");
		return;
	}

	//Add to Quantity of item
	public void AddToItem(FoodItem item, int additionAmount)
	{
		//This assumes that the Quantity of the item is an Integer
		//This turns the quantity into an integer, subtracts the
		//deduction amount and converts it back into a string
		String newQuantity = Integer.toString(
				Integer.parseInt(item.getQuantity()) + additionAmount);

		SQLiteDatabase db = this.getWritableDatabase();				//Open writable db

		String tempId = Integer.toString(item.getID());
		db.execSQL("UPDATE Groceries SET quantity = " + newQuantity + " where id = " + tempId + ";");
		return;
	}

	//Delete item
	public void deleteItem(FoodItem item)
	{
		SQLiteDatabase db = this.getWritableDatabase();												//Open db
		db.delete(TABLE_GROCERIES, KEY_ID + " = ?", new String[] { String.valueOf(item.getID())}); 	//Delete specified item
		db.close();																					//Close db
	}


}
